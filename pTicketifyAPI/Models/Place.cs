﻿using pTicketify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pTicketifyAPI.Models
{
    public class Place
    {

        public int Id { get; set; }
        public float Price { get; set; }
        public Room Room { get; set;  }
        public Show Show { get; set; }
        public int Available { get; set; }

        public Place(int id, float price, Room room, Show show, int available)
        {
            Id = id;
            Price = price;
            this.Room = room;
            this.Show = show;
            Available = available;
        }

        public Place(float price, Room room, Show show, int available)
        {
            Id = 0;
            Price = price;
            this.Room = room;
            this.Show = show;
            Available = available;
        }

        public Place()
        {
        }
    }
}