﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace pTicketify.Models
{
    public class Database
    {

        //private static readonly string CONNECTION_STRING = @"Data Source=ticketify.database.windows.net;Initial Catalog=dbTicketify;Integrated Security=False;User ID=ticketifyadmin;Password=@bc123Def5;Connect Timeout=30;Encrypt=True;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
        private static readonly string CONNECTION_STRING = @"Data Source=(localdb)\MSSQLLocalDB;Initial Catalog=DBTicketify;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";

        public static SqlConnection GetConnection()
        {

            return new SqlConnection(Database.CONNECTION_STRING);

        }

    }
}