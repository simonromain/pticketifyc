﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pTicketifyAPI.Models
{
    public class Booking
    {

        public int Id { get; set; }
        public int NbPlace { get; set; }
        public float Price { get; set; }
        public Place Place { get; set; }
        public int IdUser { get; set; }

        public Booking(int id, int nbPlace, float price, Place idPlace, int idUser)
        {
            Id = id;
            NbPlace = nbPlace;
            Price = price;
            this.Place = idPlace;
            IdUser = idUser;
        }

        public Booking(int nbPlace, float price, Place idPlace, int idUser)
        {
            Id = 0;
            NbPlace = nbPlace;
            Price = price;
            this.Place = idPlace;
            IdUser = idUser;
        }

        public Booking()
        {
        }

    }
}