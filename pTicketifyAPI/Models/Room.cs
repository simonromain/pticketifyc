﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pTicketify.Models
{
    public class Room
    {

        public int Id { get; set; }
        public int NbSeat { get; set; }
        public string NameRoom { get; set; }
        public string Adress { get; set; }
        public string ImagePath { get; set; }

        public Room(int id, int nbSeat, string nameRoom, string adress, string imagePath)
        {
            Id = id;
            NbSeat = nbSeat;
            NameRoom = nameRoom;
            Adress = adress;
            ImagePath = imagePath;
        }

        public Room(int nbSeat, string nameRoom, string adress, string imagePath)
        {
            Id = 0;
            NbSeat = nbSeat;
            NameRoom = nameRoom;
            Adress = adress;
            ImagePath = imagePath;
        }

        public Room()
        {
            
        }

    }
}