﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pTicketify.Models
{
    public class User
    {

        public int Id { get; set; }
        public string Name { get; set; }
        public string Fstname { get; set; }
        public DateTime Birth { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Street { get; set;}
        public int Number { get; set; }
        public int Zipcode { get; set; }
        public string City { get; set; }
        public string Mail { get; set; }
        public string Tel { get; set; }
        public string Language { get; set; }
        public string Title { get; set; }
        public string Country { get; set; }
        public bool IsAdmin { get; set; }

        public User(int id, string name, string fstname, DateTime birth, string login, string password, string street, int number, int zipcode, string city, string mail, string tel, string language, string title, string country, bool isAdmin)
        {
            Id = id;
            Name = name;
            Fstname = fstname;
            Birth = birth;
            Login = login;
            Password = password;
            Street = street;
            Number = number;
            Zipcode = zipcode;
            City = city;
            Mail = mail;
            Tel = tel;
            Language = language;
            Title = title;
            Country = country;
            IsAdmin = isAdmin;
        }

        public User(string name, string fstname, DateTime birth, string login, string password, string street, int number, int zipcode, string city, string mail, string tel, string language, string title, string country, bool isAdmin)
        {
            Id = 0;
            Name = name;
            Fstname = fstname;
            Birth = birth;
            Login = login;
            Password = password;
            Street = street;
            Number = number;
            Zipcode = zipcode;
            City = city;
            Mail = mail;
            Tel = tel;
            Language = language;
            Title = title;
            Country = country;
            IsAdmin = isAdmin;
        }

        public User() { }
    }
}