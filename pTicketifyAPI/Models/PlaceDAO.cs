﻿using pTicketify.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace pTicketifyAPI.Models
{
    public class PlaceDAO
    {

        private static readonly string QUERY = "SELECT * FROM place";
        private static readonly string GET = QUERY + " WHERE ID = @id";
        private static readonly string GET_TYPE = "SELECT * FROM place INNER JOIN show ON show.ID = place.ShowID" +
                                                   " WHERE show.TypeShow = @type";
        private static readonly string CREATE = "INSERT INTO place(Price, RoomID, ShowID, Available) " +
                                                "OUTPUT INSERTED.Id VALUES (@price, @room, @show, @available)";
        private static readonly string DELETE = "DELETE FROM place WHERE id = @id";
        private static readonly string UPDATE = "UPDATE place SET Price = @price, RoomID = @room, ShowID = @show, Available = @available WHERE id = @id";

        public static List<Place> getAllPlaces()
        {

            List<Place> places = new List<Place>();
            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(QUERY, connection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    places.Add(new Place(reader.GetInt32(0), reader.GetFloat(1), RoomDAO.Get(reader.GetInt32(2)), EventDAO.Get(reader.GetInt32(3)), reader.GetInt32(4)));
                connection.Close();

            }

            return places;

        }

        public static List<Place> Get(string type)
        {

            List<Place> places = new List<Place>();
            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(GET_TYPE, connection);
                command.Parameters.AddWithValue("@type", type);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    places.Add(new Place(reader.GetInt32(0), reader.GetFloat(1), RoomDAO.Get(reader.GetInt32(2)), EventDAO.Get(reader.GetInt32(3)), reader.GetInt32(4)));
                connection.Close();

            }

            return places;

        }

        public static Place Get(int id)
        {

            Place p = null;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(GET, connection);
                command.Parameters.AddWithValue("@id", id);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                    p = new Place(reader.GetInt32(0), reader.GetFloat(1), RoomDAO.Get(reader.GetInt32(2)), EventDAO.Get(reader.GetInt32(3)), reader.GetInt32(4));
                connection.Close();
            }

            return p;
        }

        public static Place Create(Place p)
        {

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(CREATE, connection);
                command.Parameters.AddWithValue("@price", p.Price);
                command.Parameters.AddWithValue("@room", p.Room.Id);
                command.Parameters.AddWithValue("@show", p.Show.Id);
                command.Parameters.AddWithValue("@available", p.Available);

                p.Id = (int)command.ExecuteScalar();

                connection.Close();
            }

            return p;

        }

        public static bool Delete(int id)
        {
            bool isDelete = false;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(DELETE, connection);
                command.Parameters.AddWithValue("@id", id);

                isDelete = command.ExecuteNonQuery() != 0;

                connection.Close();
            }

            return isDelete;

        }

        public static bool update(Place p)
        {

            bool isUpdate = false;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(UPDATE, connection);
                command.Parameters.AddWithValue("@price", p.Price);
                command.Parameters.AddWithValue("@room", p.Room.Id);
                command.Parameters.AddWithValue("@show", p.Show.Id);
                command.Parameters.AddWithValue("@available", p.Available);
                command.Parameters.AddWithValue("@id", p.Id);


                isUpdate = command.ExecuteNonQuery() != 0;

                connection.Close();
            }

            return isUpdate;

        }

    }
}