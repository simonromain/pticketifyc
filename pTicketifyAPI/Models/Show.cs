﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace pTicketify.Models
{
    public class Show
    {

        public int Id { get; set; }
        public int Popularity { get; set; }
        public string Name { get; set; }
        public DateTime Date_Deb { get; set; }
        public DateTime Date_End { get; set; }
        public string ImagePath { get; set;  }
        public string TypeShow { get; set; }

        public Show(int id, int popularity, string name, DateTime date_Deb, DateTime date_End, string imagePath, string typeShow)
        {
            Id = id;
            Popularity = popularity;
            Name = name;
            Date_Deb = date_Deb;
            Date_End = date_End;
            ImagePath = imagePath;
            TypeShow = typeShow;
        }

        public Show(int popularity, string name, DateTime date_Deb, DateTime date_End, string imagePath, string typeShow)
        {
            Id = 0;
            Popularity = popularity;
            Name = name;
            Date_Deb = date_Deb;
            Date_End = date_End;
            ImagePath = imagePath;
            TypeShow = typeShow;
        }

        public Show()
        {
           
        }
    }
}