﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace pTicketify.Models
{
    public class RoomDAO
    {

        private static readonly string QUERY = "SELECT * FROM room";
        private static readonly string GET = QUERY + " WHERE ID = @id";
        private static readonly string CREATE = "INSERT INTO room(NbSeat, NameRoom, Adress, ImagePath) " +
                                                "OUTPUT INSERTED.Id VALUES (@seat, @name, @adress, @image)";
        private static readonly string DELETE = "DELETE FROM room WHERE id = @id";
        private static readonly string UPDATE = "UPDATE room SET NbSeat = @seat, NameRoom = @name, Adress = @adress, ImagePath = @image WHERE id = @id";

        public static List<Room> getAllRooms()
        {

            List<Room> rooms = new List<Room>();
            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(QUERY, connection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    rooms.Add(new Room(reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2), reader.GetString(3), reader.GetString(4)));
                connection.Close();

            }

            return rooms;

        }


        public static Room Get(int id)
        {

            Room r = null;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(GET, connection);
                command.Parameters.AddWithValue("@id", id);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                    r = new Room(reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2), reader.GetString(3), reader.GetString(4));
                connection.Close();
            }

            return r;
        }

        public static Room Create(Room r)
        {

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(CREATE, connection);
                command.Parameters.AddWithValue("@name", r.NameRoom);
                command.Parameters.AddWithValue("@seat", r.NbSeat);
                command.Parameters.AddWithValue("@adress", r.Adress);
                command.Parameters.AddWithValue("@image", r.ImagePath);

                r.Id = (int)command.ExecuteScalar();

                connection.Close();
            }

            return r;

        }

        public static bool Delete(int id)
        {
            bool isDelete = false;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(DELETE, connection);
                command.Parameters.AddWithValue("@id", id);

                isDelete = command.ExecuteNonQuery() != 0;

                connection.Close();
            }

            return isDelete;

        }

        public static bool update(Room r)
        {

            bool isUpdate = false;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(UPDATE, connection);
                command.Parameters.AddWithValue("@name", r.NameRoom);
                command.Parameters.AddWithValue("@seat", r.NbSeat);
                command.Parameters.AddWithValue("@adress", r.Adress);
                command.Parameters.AddWithValue("@image", r.ImagePath);
                command.Parameters.AddWithValue("@id", r.Id);

                isUpdate = command.ExecuteNonQuery() != 0;

                connection.Close();
            }

            return isUpdate;

        }

    }
}