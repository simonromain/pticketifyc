﻿using pTicketify.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace pTicketifyAPI.Models
{
    public class BookingDAO
    {

        private static readonly string QUERY = "SELECT * FROM booking";
        private static readonly string GET = QUERY + " WHERE ID = @id";
        private static readonly string GETUSER = QUERY + " WHERE IdUser = @id";
        private static readonly string CREATE = "INSERT INTO booking(NbPlace, Price, IdPlace, IdUser) " +
                                                "OUTPUT INSERTED.Id VALUES (@nb, @price, @place, @user)";
        private static readonly string DELETE = "DELETE FROM booking WHERE id = @id";
        private static readonly string UPDATE = "UPDATE booking SET NbPlace = @nb, Price = @price, IdPlace = @place, IdUser = @user WHERE id = @id";

        public static List<Booking> getAllBookings()
        {

            List<Booking> bookings = new List<Booking>();
            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(QUERY, connection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    bookings.Add(new Booking(reader.GetInt32(0), reader.GetInt32(1), reader.GetFloat(2), PlaceDAO.Get(reader.GetInt32(3)), reader.GetInt32(4)));
                connection.Close();

            }

            return bookings;

        }

        public static List<Booking> getUserBookings(int id)
        {

            List<Booking> bookings = new List<Booking>();
            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(GETUSER, connection);
                command.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    bookings.Add(new Booking(reader.GetInt32(0), reader.GetInt32(1), reader.GetFloat(2), PlaceDAO.Get(reader.GetInt32(3)), reader.GetInt32(4)));
                connection.Close();

            }

            return bookings;

        }

        public static Booking Get(int id)
        {

            Booking b = null;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(GET, connection);
                command.Parameters.AddWithValue("@id", id);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                    b = new Booking(reader.GetInt32(0), reader.GetInt32(1), reader.GetFloat(2), PlaceDAO.Get(reader.GetInt32(3)), reader.GetInt32(4));
                connection.Close();
            }

            return b;
        }

        public static Booking Create(Booking b)
        {

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(CREATE, connection);
                command.Parameters.AddWithValue("@price", b.Price);
                command.Parameters.AddWithValue("@nb", b.NbPlace);
                command.Parameters.AddWithValue("@place", b.Place.Id);
                command.Parameters.AddWithValue("@user", b.IdUser);

                b.Id = (int)command.ExecuteScalar();

                connection.Close();
            }

            return b;

        }

        public static bool Delete(int id)
        {
            bool isDelete = false;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(DELETE, connection);
                command.Parameters.AddWithValue("@id", id);

                isDelete = command.ExecuteNonQuery() != 0;

                connection.Close();
            }

            return isDelete;

        }

        public static bool update(Booking b)
        {

            bool isUpdate = false;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(UPDATE, connection);
                command.Parameters.AddWithValue("@price", b.Price);
                command.Parameters.AddWithValue("@nb", b.NbPlace);
                command.Parameters.AddWithValue("@place", b.Place.Id);
                command.Parameters.AddWithValue("@user", b.IdUser);
                command.Parameters.AddWithValue("@id", b.Id);


                isUpdate = command.ExecuteNonQuery() != 0;

                connection.Close();
            }

            return isUpdate;

        }

    }
}