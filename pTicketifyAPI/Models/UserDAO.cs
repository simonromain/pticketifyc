﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace pTicketify.Models
{
    public class UserDAO
    {

        private static readonly string QUERY = "SELECT * FROM users";
        private static readonly string GET = QUERY + " WHERE LoginUser LIKE @loginUser AND PasswordUser LIKE @passwordUser";
        private static readonly string GETLOGIN = QUERY + " WHERE LoginUser LIKE @loginUser";
        private static readonly string CREATE = "INSERT INTO users(NameUser, Firstname, Birth, LoginUser, PasswordUser, Street, Number, Zipcode, " +
                                                "City, Mail, Tel, LanguageUser, Title, Country, IsAdmin) " +
                                                "OUTPUT INSERTED.Id VALUES (@name, @fstname, @birth, @login, @password, @street, @number, @zipcode, " +
                                                "@city, @mail, @tel, @language, @title, @country, @admin)";
        private static readonly string DELETE = "DELETE FROM users WHERE id = @id";
        private static readonly string UPDATE = "UPDATE users SET NameUser = @name, Firstname = @fstname, Birth = @birth, LoginUser = @login, PasswordUser = @password, " +
                                                "Street = @street, Number = @number, Zipcode = @zipcode, City = @city, Mail = @mail, Tel = @tel, LanguageUser = @language, " +
                                                "Title = @title, Country = @country, IsAdmin = @admin WHERE id = @id";

        public static List<User> getAllUsers()
        {

            List<User> users = new List<User>();
            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(QUERY, connection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    users.Add(new User(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetDateTime(3), reader.GetString(4), reader.GetString(5),
                        reader.GetString(6), reader.GetInt32(7), reader.GetInt32(8), reader.GetString(9), reader.GetString(10), reader.GetString(11), reader.GetString(12),
                        reader.GetString(13), reader.GetString(14), reader.GetBoolean(15)));
                connection.Close();

            }

                return users;

        }

       
        public static User Get(string login, string password)
        {

            User u = null;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(GET, connection);
                command.Parameters.AddWithValue("@loginUser", login);
                command.Parameters.AddWithValue("@passwordUser", password);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                    u = new User(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetDateTime(3), reader.GetString(4), reader.GetString(5),
                        reader.GetString(6), reader.GetInt32(7), reader.GetInt32(8), reader.GetString(9), reader.GetString(10), reader.GetString(11), reader.GetString(12),
                        reader.GetString(13), reader.GetString(14), reader.GetBoolean(15));
                connection.Close();
            }

            return u;
        }

        public static User Get(string login)
        {

            User u = null;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(GETLOGIN, connection);
                command.Parameters.AddWithValue("@loginUser", login);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                    u = new User(reader.GetInt32(0), reader.GetString(1), reader.GetString(2), reader.GetDateTime(3), reader.GetString(4), reader.GetString(5),
                        reader.GetString(6), reader.GetInt32(7), reader.GetInt32(8), reader.GetString(9), reader.GetString(10), reader.GetString(11), reader.GetString(12),
                        reader.GetString(13), reader.GetString(14), reader.GetBoolean(15));
                connection.Close();
            }

            return u;
        }

        public static User Create(User u)
        {

            if (Get(u.Login) != null)
            {
                using (SqlConnection connection = Database.GetConnection())
                {

                    connection.Open();
                    SqlCommand command = new SqlCommand(CREATE, connection);
                    command.Parameters.AddWithValue("@name", u.Name);
                    command.Parameters.AddWithValue("@fstname", u.Fstname);
                    command.Parameters.AddWithValue("@birth", u.Birth);
                    command.Parameters.AddWithValue("@login", u.Login);
                    command.Parameters.AddWithValue("@password", u.Password);
                    command.Parameters.AddWithValue("@street", u.Street);
                    command.Parameters.AddWithValue("@number", u.Number);
                    command.Parameters.AddWithValue("@zipcode", u.Zipcode);
                    command.Parameters.AddWithValue("@city", u.City);
                    command.Parameters.AddWithValue("@mail", u.Mail);
                    command.Parameters.AddWithValue("@tel", u.Tel);
                    command.Parameters.AddWithValue("@language", u.Language);
                    command.Parameters.AddWithValue("@title", u.Title);
                    command.Parameters.AddWithValue("@country", u.Country);
                    command.Parameters.AddWithValue("@admin", u.IsAdmin);

                    u.Id = (int)command.ExecuteScalar();

                    connection.Close();
                }

                return u;

            }

            return null;

        }

        public static bool Delete(int id)
        {
            bool isDelete = false;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(DELETE, connection);
                command.Parameters.AddWithValue("@id", id);

                isDelete = command.ExecuteNonQuery() != 0;

                connection.Close();
            }

            return isDelete;

        }

        public static bool update(User u)
        {

            bool isUpdate = false;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(UPDATE, connection);
                command.Parameters.AddWithValue("@name", u.Name);
                command.Parameters.AddWithValue("@fstname", u.Fstname);
                command.Parameters.AddWithValue("@birth", u.Birth);
                command.Parameters.AddWithValue("@login", u.Login);
                command.Parameters.AddWithValue("@password", u.Password);
                command.Parameters.AddWithValue("@street", u.Street);
                command.Parameters.AddWithValue("@number", u.Number);
                command.Parameters.AddWithValue("@zipcode", u.Zipcode);
                command.Parameters.AddWithValue("@city", u.City);
                command.Parameters.AddWithValue("@mail", u.Mail);
                command.Parameters.AddWithValue("@tel", u.Tel);
                command.Parameters.AddWithValue("@language", u.Language);
                command.Parameters.AddWithValue("@title", u.Title);
                command.Parameters.AddWithValue("@country", u.Country);
                command.Parameters.AddWithValue("@admin", u.IsAdmin);
                command.Parameters.AddWithValue("@id", u.Id);

                isUpdate = command.ExecuteNonQuery() != 0;

                connection.Close();
            }

            return isUpdate;

        }

    }
}