﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace pTicketify.Models
{
    public class EventDAO
    {

        private static readonly string QUERY = "SELECT * FROM show";
        private static readonly string GET = QUERY + " WHERE ID = @id";
        private static readonly string CREATE = "INSERT INTO show(NameShow, Popularity, DateBeg, DateEnd, ImagePath, TypeShow) " +
                                                "OUTPUT INSERTED.Id VALUES (@name, @popularity, @beg, @end, @image, @typeShow)";
        private static readonly string DELETE = "DELETE FROM show WHERE id = @id";
        private static readonly string UPDATE = "UPDATE show SET NameShow = @name, Popularity = @popularity, DateBeg = @beg, DateEnd = @end, ImagePath = @image, TypeShow = @typeShow WHERE id = @id";

        public static List<Show> getAllShows()
        {

            List<Show> shows = new List<Show>();
            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(QUERY, connection);
                SqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                    shows.Add(new Show(reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2), reader.GetDateTime(3), reader.GetDateTime(4), reader.GetString(5), reader.GetString(6)));
                connection.Close();

            }

            return shows;

        }


        public static Show Get(int id)
        {

            Show s = null;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(GET, connection);
                command.Parameters.AddWithValue("@id", id);

                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                    s = new Show(reader.GetInt32(0), reader.GetInt32(1), reader.GetString(2), reader.GetDateTime(3), reader.GetDateTime(4), reader.GetString(5), reader.GetString(6));
                connection.Close();
            }

            return s;
        }

        public static Show Create(Show s)
        {

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(CREATE, connection);
                command.Parameters.AddWithValue("@name", s.Name);
                command.Parameters.AddWithValue("@popularity", s.Popularity);
                command.Parameters.AddWithValue("@beg", s.Date_Deb);
                command.Parameters.AddWithValue("@end", s.Date_End);
                command.Parameters.AddWithValue("@image", s.ImagePath);
                command.Parameters.AddWithValue("@typeShow", s.TypeShow);

                s.Id = (int)command.ExecuteScalar();

                connection.Close();
            }

            return s;

        }

        public static bool Delete(int id)
        {
            bool isDelete = false;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(DELETE, connection);
                command.Parameters.AddWithValue("@id", id);

                isDelete = command.ExecuteNonQuery() != 0;

                connection.Close();
            }

            return isDelete;

        }

        public static bool update(Show s)
        {

            bool isUpdate = false;

            using (SqlConnection connection = Database.GetConnection())
            {

                connection.Open();
                SqlCommand command = new SqlCommand(UPDATE, connection);
                command.Parameters.AddWithValue("@name", s.Name);
                command.Parameters.AddWithValue("@popularity", s.Popularity);
                command.Parameters.AddWithValue("@beg", s.Date_Deb);
                command.Parameters.AddWithValue("@end", s.Date_End);
                command.Parameters.AddWithValue("@image", s.ImagePath);
                command.Parameters.AddWithValue("@typeShow", s.TypeShow);
                command.Parameters.AddWithValue("@id", s.Id);

                isUpdate = command.ExecuteNonQuery() != 0;

                connection.Close();
            }

            return isUpdate;

        }

    }
}