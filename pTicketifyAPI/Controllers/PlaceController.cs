﻿using pTicketifyAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace pTicketifyAPI.Controllers
{
    public class PlaceController : ApiController
    {

        public IEnumerable<Place> Get() { return PlaceDAO.getAllPlaces(); }
        public IEnumerable<Place> Get(string type) { return PlaceDAO.Get(type); }
        public Place Get(int id) { return PlaceDAO.Get(id); }
        public Place Post(Place p)
        {
            return PlaceDAO.Create(p);
        }

        public IHttpActionResult Put(Place p)
        {

            if (PlaceDAO.update(p))
                return Ok();
            return BadRequest();

        }

        public IHttpActionResult Delete(int id)
        {

            if (PlaceDAO.Delete(id))
                return Ok();
            return BadRequest();

        }

    }
}
