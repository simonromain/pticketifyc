﻿using pTicketifyAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace pTicketifyAPI.Controllers
{
    public class BookingController : ApiController
    {
        public IEnumerable<Booking> Get() { return BookingDAO.getAllBookings(); }
        public IEnumerable<Booking> Get(int id) { return BookingDAO.getUserBookings(id); }
       // public Booking Get(int id) { return BookingDAO.Get(id); }
        public Booking Post(Booking b)
        {
            return BookingDAO.Create(b);
        }

        public IHttpActionResult Put(Booking b)
        {

            if (BookingDAO.update(b))
                return Ok();
            return BadRequest();

        }

        public IHttpActionResult Delete(int id)
        {

            if (BookingDAO.Delete(id))
                return Ok();
            return BadRequest();

        }
    }
}
