﻿
using pTicketify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace pTicketify.Controllers
{
    public class EventController : ApiController
    {

        //http://127.0.0.1:000/api/event

        public IEnumerable<Show> Get() { return EventDAO.getAllShows(); }
        public Show Get(int id) { return EventDAO.Get(id); }
        public Show Post(Show s) {
            return EventDAO.Create(s);
            }

        public IHttpActionResult Put(Show s)
        {

            if (EventDAO.update(s))
                return Ok();
            return BadRequest();

        }

        public IHttpActionResult Delete(int id)
        {

            if (EventDAO.Delete(id))
                return Ok();
            return BadRequest();

        }

    }
}
