﻿using pTicketify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace pTicketify.Controllers
{
    public class RoomController : ApiController
    {
        public IEnumerable<Room> Get() { return RoomDAO.getAllRooms(); }
        public Room Get(int id) { return RoomDAO.Get(id); }
        public Room Post(Room r)
        {
            return RoomDAO.Create(r);
        }

        public IHttpActionResult Put(Room u)
        {

            if (RoomDAO.update(u))
                return Ok();
            return BadRequest();

        }

        public IHttpActionResult Delete(int id)
        {

            if (RoomDAO.Delete(id))
                return Ok();
            return BadRequest();

        }
    }
}
