﻿using pTicketify.Models;
using pTicketifyAPI.JWT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace pTicketifyAPI.Controllers
{
    public class TokenController : ApiController
    {

        public string Get(string username, string password)
        {

            User u = CheckUser(username, password);

            if (u != null)
            {
                return JWTManager.GenerateToken(username, u.IsAdmin);
            }

            return "Unauthorized";  
        }

        public User CheckUser(string username, string password)
        {

            return UserDAO.Get(username, password);

        }

    }
}
