﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using pTicketifyAPI.JWT;

namespace pTicketifyAPI.Controllers
{
    public class ValuesController : ApiController
    {

        [JwtAuthentication]
        public string Get()
        {
            return "value";
        }

    }
}
