﻿using pTicketify.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace pTicketify.Controllers
{
    public class UserController : ApiController
    {

        public IEnumerable<User> Get() { return UserDAO.getAllUsers(); }
        public User Get(string login, string password) { return UserDAO.Get(login, password); }
        public User Get(string login) { return UserDAO.Get(login); }
        public User Post(User u)
        {
            return UserDAO.Create(u);
        }

        public IHttpActionResult Put(User u)
        {

            if (UserDAO.update(u))
                return Ok();
            return BadRequest();

        }

        public IHttpActionResult Delete(int id)
        {

            if (UserDAO.Delete(id))
                return Ok();
            return BadRequest();

        }

    }
}
